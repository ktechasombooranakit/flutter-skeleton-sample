import 'package:flutter/material.dart';
import 'package:mobile/constants/style.dart';
import 'package:mobile/utils/extensions.dart';

class ContentDetailHeader extends StatelessWidget {
  final String title;
  final Duration duration;

  ContentDetailHeader({@required this.title, @required this.duration});

  @override
  Widget build(BuildContext context) {
    var durationString = DurationExtensions.toStringFormat(duration, "HH:mm");
    return Padding(
      padding: EdgeInsets.all(defaultPadding),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: defaultHeaderTextStyle,
          ),
          SizedBox(
            height: textLinePadding,
          ),
          Text(
            "Total duration $durationString Hrs.",
            style: defaultDescriptionTextStyle,
          ),
        ],
      ),
    );
  }
}
