import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:mobile/views/layouts/video_view_layout.dart';
import 'package:mobile/views/molecules/video_panel.dart';
import 'package:mobile/views/organisms/lesson_tab_view.dart';

class LessonPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _LessonPageState();
  }
}

class _LessonPageState extends State<LessonPage> {
  @override
  Widget build(BuildContext context) {
    return VideoViewLayout(
      video: VideoPanel(),
      body: LessonTabView(),
    );
  }
}
