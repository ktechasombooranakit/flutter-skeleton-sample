import 'package:flutter/material.dart';
import 'package:mobile/constants/style.dart';

class ContentSectionListItem extends StatelessWidget {
  final int number;
  final String title;
  final Duration duration;

  ContentSectionListItem(
      {@required this.number, @required this.title, this.duration});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(defaultPadding),
      child: Row(
        children: [
          Text(
            "Section $number - $title",
            style: defaultDescriptionTextStyle,
          ),
        ],
      ),
    );
  }
}
