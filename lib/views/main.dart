import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mobile/constants/style.dart';
import 'package:mobile/views/pages/lesson_page.dart';
import 'package:mobile/views/pages/main_page.dart';

class MainApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Material(
      child: CupertinoApp(
        localizationsDelegates: const <LocalizationsDelegate<dynamic>>[
          DefaultMaterialLocalizations.delegate,
          DefaultWidgetsLocalizations.delegate,
        ],
        initialRoute: "/",
        onGenerateRoute: (routeSettings) {
          switch (routeSettings.name) {
            case "/lesson":
              return CupertinoPageRoute(
                builder: (context) => LessonPage(),
                fullscreenDialog: true,
              );
            default:
              return null;
          }
        },
        home: MainPage(),
        theme: CupertinoThemeData(
          scaffoldBackgroundColor: backgroundColor,
          barBackgroundColor: foregroundColor,
          primaryColor: mainTextColor,
        ),
      ),
    );
  }
}
