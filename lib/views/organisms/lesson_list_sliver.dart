import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:mobile/stores/current_lesson_store.dart';
import 'package:mobile/stores/lessons_store.dart';
import 'package:mobile/constants/style.dart';
import 'package:mobile/views/molecules/cover_card.dart';
import 'package:provider/provider.dart';

class LessonListSliver extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var lessonsViewStore = Provider.of<LessonsStore>(context);

    return Observer(
      builder: (_) {
        return SliverGrid.count(
          childAspectRatio: 20 / 11,
          crossAxisCount: 2,
          mainAxisSpacing: gridPadding,
          crossAxisSpacing: gridPadding,
          children: List.generate(lessonsViewStore.length, (index) {
            var item = lessonsViewStore.items[index];
            return CoverCard(
              title: item.title,
              color: item.color,
              coveredImage: item.coveredImage,
              onPress: () {
                var currentLessonStore =
                    Provider.of<CurrentLessonStore>(context);
                currentLessonStore.setAndFetchCurrentLesson(item.id);
                Navigator.of(context, rootNavigator: true).pushNamed("/lesson");
              },
            );
          }),
        );
      },
    );
  }
}
