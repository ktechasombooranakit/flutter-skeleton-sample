import 'package:flutter/material.dart';
import 'package:mobx/mobx.dart';
part 'lessons_store.g.dart';

class LessonsStore = _LessonsStore with _$LessonsStore;

abstract class _LessonsStore with Store {
  @observable
  ObservableFuture<List<LessonItemView>> itemsFuture;

  @observable
  bool isRequestFetch;

  @computed
  bool get isLoaded =>
      itemsFuture != null && itemsFuture.status == FutureStatus.fulfilled;

  @computed
  List<LessonItemView> get items {
    if (!isLoaded) {
      return [];
    }
    return itemsFuture.value;
  }

  @computed
  int get length => items.length;

  @action
  fetch() {
    isRequestFetch = true;
  }

  @action
  setFetchFuture(Future<List<LessonItemView>> future) {
    isRequestFetch = false;
    itemsFuture = ObservableFuture(future);
  }
}

class LessonItemView {
  final String id;
  final String title;
  final Color color;
  final Image coveredImage;

  LessonItemView({
    this.id = "",
    this.color = Colors.grey,
    this.title = "undefined",
    this.coveredImage,
  });
}
