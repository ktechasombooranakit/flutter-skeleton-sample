import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:mobile/constants/style.dart';
import 'package:mobile/stores/current_lesson_store.dart';
import 'package:mobile/views/molecules/default_tabs.dart';
import 'package:mobile/views/molecules/content_detail_header.dart';

import 'dart:math' as math;

import 'package:mobile/views/organisms/lesson_cheatsheet_list_view.dart';
import 'package:mobile/views/organisms/lesson_content_list_view.dart';
import 'package:mobile/views/organisms/lesson_summary_list_view.dart';
import 'package:provider/provider.dart';

class LessonTabView extends StatefulWidget {
  @override
  _LessonTabViewState createState() => _LessonTabViewState();
}

class _LessonTabViewState extends State<LessonTabView> {
  List<String> _tabs;

  @override
  void initState() {
    super.initState();
    _tabs = ["Lesson", "Summary", "Cheatsheet"];
  }

  @override
  Widget build(BuildContext context) {
    var currentLessonStore = Provider.of<CurrentLessonStore>(context);
    return DefaultTabController(
      length: _tabs.length,
      child: NestedScrollView(
        physics: BouncingScrollPhysics(),
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return [
            SliverToBoxAdapter(
              child: Observer(
                builder: (context) => ContentDetailHeader(
                  title: currentLessonStore.title,
                  duration: currentLessonStore.totalDuration,
                ),
              ),
            ),
            SliverPersistentHeader(
              pinned: true,
              delegate: _SliverAppBarDelegate.nonExtent(
                height: 40,
                child: Container(
                  color: backgroundColor,
                  child: DefaultTabBar(tabs: _tabs),
                ),
              ),
            ),
          ];
        },
        body: TabBarView(
          children: [
            LessonContentListView(),
            LessonSummaryListView(),
            LessonCheatsheetListView(),
          ],
        ),
      ),
    );
  }
}

class _SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  _SliverAppBarDelegate({
    @required this.minHeight,
    @required this.maxHeight,
    @required this.child,
  });

  _SliverAppBarDelegate.nonExtent(
      {@required double height, @required this.child})
      : this.minHeight = height,
        this.maxHeight = height;

  final double minHeight;
  final double maxHeight;
  final Widget child;
  @override
  double get minExtent => minHeight;
  @override
  double get maxExtent => math.max(maxHeight, minHeight);
  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return new SizedBox.expand(child: child);
  }

  @override
  bool shouldRebuild(_SliverAppBarDelegate oldDelegate) {
    return maxHeight != oldDelegate.maxHeight ||
        minHeight != oldDelegate.minHeight ||
        child != oldDelegate.child;
  }
}
