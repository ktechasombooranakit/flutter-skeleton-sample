import 'package:flutter/material.dart';
import 'package:mobile/sagas/lessons_fetcher_saga.dart';
import 'package:mobile/sagas/saga.dart';
import 'package:mobile/sagas/startup_saga.dart';
import 'package:mobile/stores/app_store.dart';
import 'package:mobile/stores/current_lesson_store.dart';
import 'package:mobile/stores/lessons_store.dart';

// TODO: We should use dependency injection instead
class MainSaga extends MultiSaga {
  MainSaga({
    @required AppStore appStore,
    @required LessonsStore lessonsStore,
    @required CurrentLessonStore currentLessonStore,
  }) : super(
          sagas: [
            StartupSaga(
              lessonsStore: lessonsStore,
              appStore: appStore,
            ),
            LessonsFetcherSaga(
              lessonsStore: lessonsStore,
              currentLessonStore: currentLessonStore,
            ),
          ],
        );
}
