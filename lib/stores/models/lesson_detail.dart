class LessonDetail {
  LessonDetail({
    this.title,
    this.totalDuration,
    this.content,
    this.summary,
    this.cheatsheet,
  });

  final String title;
  final Duration totalDuration;
  final Content content;
  final Summary summary;
  final Cheatsheet cheatsheet;
}

class Content {
  Content({this.sections});

  final List<ContentSection> sections;
}

class ContentSection {
  ContentSection({this.items, this.indexNumber, this.title});

  final int indexNumber;
  final String title;
  final List<ContentItem> items;
}

class ContentItem {
  ContentItem({this.videoId, this.indexNumber, this.title, this.duration});

  final int indexNumber;
  final String title;
  final String videoId;
  final Duration duration;
}

class Summary {
  Summary({this.items});

  final List<SummaryItem> items;
}

class SummaryItem {
  SummaryItem({this.indexNumber, this.title, this.videoId});

  final int indexNumber;
  final String title;
  final String videoId;
}

class Cheatsheet {
  Cheatsheet({this.items});

  final List<CheatsheetItem> items;
}

class CheatsheetItem {
  CheatsheetItem(
      {this.indexNumber, this.title, this.videoId, this.cheatsheetIds});

  final int indexNumber;
  final String title;
  final int videoId;
  final List<String> cheatsheetIds;
}
