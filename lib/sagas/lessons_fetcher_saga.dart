import 'dart:async';

import 'package:flutter/material.dart';
import 'package:mobile/sagas/saga.dart';
import 'package:mobile/stores/current_lesson_store.dart';
import 'package:mobile/stores/lessons_store.dart';
import 'package:mobile/stores/models/lesson_detail.dart';
import 'package:mobile/utils/extensions.dart';
import 'package:mobx/mobx.dart';

class LessonsFetcherSaga extends Saga {
  final LessonsStore lessonsStore;
  final CurrentLessonStore currentLessonStore;

  LessonsFetcherSaga({
    @required this.lessonsStore,
    @required this.currentLessonStore,
  });

  @override
  void start() {
    trackRequestFetchLesson();
    trackSetCurrentLesson();
  }

  void trackRequestFetchLesson() {
    when(
      (_) => lessonsStore.isRequestFetch,
      () {
        var future = loadLessons();
        lessonsStore.setFetchFuture(future);
      },
    );
  }

  // Here is fake code
  Future<List<LessonItemView>> loadLessons() {
    var completer = Completer<List<LessonItemView>>();
    completer.complete(
      List.generate(14, (index) {
        return LessonItemView(
          id: "$index",
          title: "This is $index",
          color: ColorExtensions.randomColor(),
          coveredImage: Image.network(
              "https://lh3.googleusercontent.com/coMv1dl31PCfEs6essJoEUwVryaqKHKQvENdZ_WYpN-PXa8Qfitkg3grQxIVN22W5A"),
        );
      }),
    );
    return completer.future;
  }

  void trackSetCurrentLesson() {
    reaction<String>(
      (_) => currentLessonStore.currentLessonId,
      (currentLessonId) {
        final future = loadLessonDetail(currentLessonId);
        currentLessonStore.setCurrentLessonFuture(future);
      },
    );
  }

  // Here is fake code
  Future<LessonDetail> loadLessonDetail(String lessonId) {
    var completer = new Completer<LessonDetail>();
    completer.complete(
      LessonDetail(
        cheatsheet: Cheatsheet(),
        summary: Summary(),
        totalDuration: Duration(minutes: 1000),
        title: "Lesson titles",
        content: Content(
          sections: List.generate(
            4,
            (index) => ContentSection(
              indexNumber: index + 1,
              items: List.generate(
                5,
                (index) => ContentItem(
                  indexNumber: index + 1,
                  title: "content name",
                  videoId: "$index",
                  duration: Duration(minutes: 20),
                ),
              ),
              title: "Section name",
            ),
          ),
        ),
      ),
    );
    return completer.future;
  }
}
