import 'package:flutter/material.dart';
import 'package:mobile/constants/style.dart';

class TitleScrollableView extends StatelessWidget {
  final Widget sliver;
  final String title;
  final ScrollController scrollController;

  const TitleScrollableView(
      {@required this.title, @required this.sliver, this.scrollController});

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      controller: scrollController,
      slivers: [
        silverGap,
        SliverPadding(
          padding: EdgeInsets.symmetric(horizontal: defaultPadding),
          sliver: SliverToBoxAdapter(
            child: Text(
              title,
              style: titleTextStyle,
            ),
          ),
        ),
        silverGap,
        SliverPadding(
          padding: EdgeInsets.symmetric(horizontal: defaultPadding),
          sliver: sliver,
        ),
        silverGap,
      ],
    );
  }

  Widget get silverGap {
    return SliverToBoxAdapter(child: Container(height: defaultPadding));
  }
}
