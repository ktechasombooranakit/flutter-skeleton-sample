import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:mobile/stores/current_lesson_store.dart';
import 'package:mobile/stores/models/lesson_detail.dart';
import 'package:mobile/views/molecules/list_item/content_section_list_item.dart';
import 'package:mobile/views/molecules/list_item/content_list_item.dart';
import 'package:provider/provider.dart';

class LessonContentListView extends StatelessWidget {
  Widget buildScrollView(BuildContext context, Content content) {
    List<Widget> slivers = [];
    final sections = content.sections;
    sections.forEach((section) {
      slivers.add(
        ContentSectionListItem(
          title: section.title,
          number: section.indexNumber,
          duration: Duration(), // Not implemeted
        ),
      );
      final items = section.items;
      items.forEach((item) {
        slivers.add(
          ContentListItem(
            title: item.title,
            number: item.indexNumber,
            duration: item.duration,
          ),
        );
      });
    });

    return CustomScrollView(
      slivers: <Widget>[
        SliverList(
          delegate: SliverChildBuilderDelegate(
            (BuildContext context, int index) {
              if (index >= slivers.length) {
                return null;
              }
              return slivers[index];
            },
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Observer(
      builder: (context) => buildContent(context),
    );
  }

  Widget buildContent(BuildContext context) {
    final store = Provider.of<CurrentLessonStore>(context);
    if (!store.isLoaded) {
      return Container();
    }
    return buildScrollView(context, store.content);
  }
}
