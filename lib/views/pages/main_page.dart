import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:mobile/views/layouts/bottom_tabbar_layout.dart';
import 'package:mobile/views/mixins/tab_navigator.dart';
import 'package:mobile/views/pages/lesson_list_page.dart';

class MainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BottomTabbarLayout(
      tabItems: [
        BottomNavigationBarItem(
          activeIcon: Icon(CupertinoIcons.book_solid),
          icon: Icon(CupertinoIcons.book),
          title: Text("Lessons"),
        ),
        BottomNavigationBarItem(
            activeIcon: Icon(CupertinoIcons.person_solid),
            icon: Icon(CupertinoIcons.person),
            title: Text("My Account")),
        BottomNavigationBarItem(
          activeIcon: Icon(CupertinoIcons.person_solid),
          icon: Icon(CupertinoIcons.person),
          title: Text("Referal"),
        ),
      ],
      builder: (context, navigationController, index) {
        switch (index) {
          case 0:
            return TabNavigatorView(
              controller: navigationController,
              builder: (context, scrollController) =>
                  LessonListPage(scrollController: scrollController),
            );
            break;
          case 1:
            return TabNavigatorView(
              controller: navigationController,
              builder: (context, scrollController) =>
                  LessonListPage(scrollController: scrollController),
            );
            break;
          case 2:
            return TabNavigatorView(
              controller: navigationController,
              builder: (context, scrollController) =>
                  LessonListPage(scrollController: scrollController),
            );
            break;
          default:
            return null;
        }
      },
    );
  }
}
