import 'package:flutter/material.dart';

class DefaultTabBar extends StatelessWidget {
  final List<String> tabs;

  DefaultTabBar({@required this.tabs});

  @override
  Widget build(BuildContext context) {
    return TabBar(
      tabs: tabs.map((String name) => Tab(text: name)).toList(),
      indicatorSize: TabBarIndicatorSize.tab,
      labelPadding: EdgeInsets.symmetric(horizontal: 0),
      indicator: BoxDecoration(color: Colors.amber),
      indicatorWeight: 0,
    );
  }
}
