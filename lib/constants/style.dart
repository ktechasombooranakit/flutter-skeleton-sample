import 'package:flutter/material.dart';
// import 'package:flutter/cupertino.dart';

// Colors
var backgroundColor = Colors.black;
var foregroundColor = Colors.grey[900];

// Text Colors
var mainTextColor = Colors.white;
var secondaryTextColor = Colors.white60;

// Padding
var defaultPadding = 16.0;
var gridPadding = 15.0;
var textLinePadding = 8.0;

// Sizing
var defaultCardRadius = 4.0;
var defaultIconSize = 28.0;

var titleTextStyle = TextStyle(
  color: mainTextColor,
  fontSize: 35,
  fontWeight: FontWeight.bold,
  letterSpacing: 3,
);

var subTitleTextStyle = TextStyle(
  color: mainTextColor,
  fontSize: 18,
  fontWeight: FontWeight.bold,
  letterSpacing: 2,
);

var defaultTextStyle = TextStyle(
  color: mainTextColor,
  fontSize: 16,
  letterSpacing: 1,
);

var defaultHeaderTextStyle = TextStyle(
  color: mainTextColor,
  fontSize: 25,
  letterSpacing: 0.5,
);

var defaultDescriptionTextStyle = TextStyle(
  color: secondaryTextColor,
  fontSize: 14,
  letterSpacing: 0.5,
);
