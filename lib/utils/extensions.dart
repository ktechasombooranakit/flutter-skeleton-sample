/* 
  TODO: Extensions method feature in Dart will arrived in near future.
  At that point we will need to refactor these code.
*/

import 'dart:math';
import 'package:flutter/material.dart';

class IntExtensions {
  static String toStringLeadingZero(int number, int digit) {
    return number.toString().padLeft(digit, '0');
  }
}

class DurationExtensions {
  static String toStringFormat(Duration duration, String format) {
    assert(duration != null);
    assert(format != null);

    String replaceRange(String source, String character, int number) {
      return source.replaceAllMapped(RegExp("($character+)"), (match) {
        var digit = match.group(0).length;
        return IntExtensions.toStringLeadingZero(number, digit);
      });
    }

    var resultString = format;

    final hours = duration.inHours.remainder(60);
    resultString = replaceRange(resultString, "H", hours);

    final minutes = duration.inMinutes.remainder(60);
    resultString = replaceRange(resultString, "m", minutes);

    final seconds = duration.inSeconds.remainder(60);
    resultString = replaceRange(resultString, "s", seconds);

    return resultString;
  }
}

class ColorExtensions {
  static Color randomColor() {
    return Color((Random().nextDouble() * 0xFFFFFF).toInt() << 0)
        .withOpacity(1.0);
  }
}
