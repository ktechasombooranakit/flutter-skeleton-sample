// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'current_lesson_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$CurrentLessonStore on _CurrentLessonStore, Store {
  Computed<bool> _$hasCurrentLessonComputed;

  @override
  bool get hasCurrentLesson => (_$hasCurrentLessonComputed ??=
          Computed<bool>(() => super.hasCurrentLesson))
      .value;
  Computed<bool> _$isLoadedComputed;

  @override
  bool get isLoaded =>
      (_$isLoadedComputed ??= Computed<bool>(() => super.isLoaded)).value;
  Computed<LessonDetail> _$lessonDetailComputed;

  @override
  LessonDetail get lessonDetail => (_$lessonDetailComputed ??=
          Computed<LessonDetail>(() => super.lessonDetail))
      .value;
  Computed<Summary> _$summaryComputed;

  @override
  Summary get summary =>
      (_$summaryComputed ??= Computed<Summary>(() => super.summary)).value;
  Computed<Content> _$contentComputed;

  @override
  Content get content =>
      (_$contentComputed ??= Computed<Content>(() => super.content)).value;
  Computed<Cheatsheet> _$cheatsheetComputed;

  @override
  Cheatsheet get cheatsheet =>
      (_$cheatsheetComputed ??= Computed<Cheatsheet>(() => super.cheatsheet))
          .value;
  Computed<String> _$titleComputed;

  @override
  String get title =>
      (_$titleComputed ??= Computed<String>(() => super.title)).value;
  Computed<Duration> _$totalDurationComputed;

  @override
  Duration get totalDuration => (_$totalDurationComputed ??=
          Computed<Duration>(() => super.totalDuration))
      .value;

  final _$currentLessonIdAtom =
      Atom(name: '_CurrentLessonStore.currentLessonId');

  @override
  String get currentLessonId {
    _$currentLessonIdAtom.context.enforceReadPolicy(_$currentLessonIdAtom);
    _$currentLessonIdAtom.reportObserved();
    return super.currentLessonId;
  }

  @override
  set currentLessonId(String value) {
    _$currentLessonIdAtom.context.conditionallyRunInAction(() {
      super.currentLessonId = value;
      _$currentLessonIdAtom.reportChanged();
    }, _$currentLessonIdAtom, name: '${_$currentLessonIdAtom.name}_set');
  }

  final _$lessonDetailFutureAtom =
      Atom(name: '_CurrentLessonStore.lessonDetailFuture');

  @override
  ObservableFuture<LessonDetail> get lessonDetailFuture {
    _$lessonDetailFutureAtom.context
        .enforceReadPolicy(_$lessonDetailFutureAtom);
    _$lessonDetailFutureAtom.reportObserved();
    return super.lessonDetailFuture;
  }

  @override
  set lessonDetailFuture(ObservableFuture<LessonDetail> value) {
    _$lessonDetailFutureAtom.context.conditionallyRunInAction(() {
      super.lessonDetailFuture = value;
      _$lessonDetailFutureAtom.reportChanged();
    }, _$lessonDetailFutureAtom, name: '${_$lessonDetailFutureAtom.name}_set');
  }

  final _$_CurrentLessonStoreActionController =
      ActionController(name: '_CurrentLessonStore');

  @override
  void setAndFetchCurrentLesson(String lessonId) {
    final _$actionInfo = _$_CurrentLessonStoreActionController.startAction();
    try {
      return super.setAndFetchCurrentLesson(lessonId);
    } finally {
      _$_CurrentLessonStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setCurrentLessonFuture(Future<LessonDetail> future) {
    final _$actionInfo = _$_CurrentLessonStoreActionController.startAction();
    try {
      return super.setCurrentLessonFuture(future);
    } finally {
      _$_CurrentLessonStoreActionController.endAction(_$actionInfo);
    }
  }
}
