import 'package:flutter/material.dart';
import 'package:mobile/constants/style.dart';
import 'package:mobile/views/atoms/button.dart';

class VideoPanel extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Positioned.fill(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              AspectRatio(
                aspectRatio: 16 / 9,
                child: Container(),
              )
            ],
          ),
        ),
        Positioned(
          child: Padding(
            padding: EdgeInsets.all(defaultPadding),
            child: Row(
              children: [
                Button(
                  onPress: () => Navigator.of(context).pop(),
                  child: Icon(
                    Icons.arrow_downward,
                    color: Colors.white,
                    size: defaultIconSize,
                  ),
                ),
              ],
            ),
          ),
        )
      ],
    );
  }
}
