// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$AppStore on _AppStore, Store {
  final _$hasStartupAtom = Atom(name: '_AppStore.hasStartup');

  @override
  bool get hasStartup {
    _$hasStartupAtom.context.enforceReadPolicy(_$hasStartupAtom);
    _$hasStartupAtom.reportObserved();
    return super.hasStartup;
  }

  @override
  set hasStartup(bool value) {
    _$hasStartupAtom.context.conditionallyRunInAction(() {
      super.hasStartup = value;
      _$hasStartupAtom.reportChanged();
    }, _$hasStartupAtom, name: '${_$hasStartupAtom.name}_set');
  }

  final _$_AppStoreActionController = ActionController(name: '_AppStore');

  @override
  void startup() {
    final _$actionInfo = _$_AppStoreActionController.startAction();
    try {
      return super.startup();
    } finally {
      _$_AppStoreActionController.endAction(_$actionInfo);
    }
  }
}
