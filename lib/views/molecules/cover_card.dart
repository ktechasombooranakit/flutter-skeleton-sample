import 'package:flutter/material.dart';
import 'package:mobile/constants/style.dart';
import 'package:mobile/views/atoms/button.dart';

class CoverCard extends StatelessWidget {
  final Color color;
  final Image coveredImage;
  final String title;
  final Function onPress;

  CoverCard(
      {this.title, this.color = Colors.grey, this.coveredImage, this.onPress});

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(defaultCardRadius),
      child: Button(
        onPress: onPress,
        child: Container(
          color: color,
          child: Stack(
            children: [
              Positioned(
                bottom: 0,
                right: 0,
                child: SizedBox(
                  width: 100,
                  height: 100,
                  child: coveredImage,
                ),
              ),
              Positioned(
                top: gridPadding - 2,
                left: gridPadding - 2,
                child: Text("Demo Title", style: subTitleTextStyle),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
