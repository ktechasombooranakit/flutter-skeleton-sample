import 'package:flutter/material.dart';
import 'package:mobile/views/molecules/title_scrollable_view.dart';
import 'package:mobile/views/organisms/lesson_list_sliver.dart';

class LessonListPage extends StatelessWidget {
  final ScrollController scrollController;

  LessonListPage({this.scrollController});

  @override
  Widget build(BuildContext context) {
    return TitleScrollableView(
      scrollController: scrollController,
      title: "Lessons",
      sliver: LessonListSliver(),
    );
  }
}
