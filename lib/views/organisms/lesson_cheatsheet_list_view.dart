import 'package:flutter/material.dart';
import 'package:mobile/views/molecules/list_item/content_cheatsheet_list_item.dart';

class LessonCheatsheetListView extends StatelessWidget {
  // TODO: Connect to the store.
  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: <Widget>[
        SliverList(
          delegate: SliverChildBuilderDelegate(
            (BuildContext context, int index) {
              if (index > 5) return null;
              return ContentCheatsheetListItem(
                title: "Test Title",
                number: index,
                texs: ["", "", ""],
              );
            },
          ),
        ),
      ],
    );
  }
}
