// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'lessons_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$LessonsStore on _LessonsStore, Store {
  Computed<bool> _$isLoadedComputed;

  @override
  bool get isLoaded =>
      (_$isLoadedComputed ??= Computed<bool>(() => super.isLoaded)).value;
  Computed<List<LessonItemView>> _$itemsComputed;

  @override
  List<LessonItemView> get items =>
      (_$itemsComputed ??= Computed<List<LessonItemView>>(() => super.items))
          .value;
  Computed<int> _$lengthComputed;

  @override
  int get length =>
      (_$lengthComputed ??= Computed<int>(() => super.length)).value;

  final _$itemsFutureAtom = Atom(name: '_LessonsStore.itemsFuture');

  @override
  ObservableFuture<List<LessonItemView>> get itemsFuture {
    _$itemsFutureAtom.context.enforceReadPolicy(_$itemsFutureAtom);
    _$itemsFutureAtom.reportObserved();
    return super.itemsFuture;
  }

  @override
  set itemsFuture(ObservableFuture<List<LessonItemView>> value) {
    _$itemsFutureAtom.context.conditionallyRunInAction(() {
      super.itemsFuture = value;
      _$itemsFutureAtom.reportChanged();
    }, _$itemsFutureAtom, name: '${_$itemsFutureAtom.name}_set');
  }

  final _$isRequestFetchAtom = Atom(name: '_LessonsStore.isRequestFetch');

  @override
  bool get isRequestFetch {
    _$isRequestFetchAtom.context.enforceReadPolicy(_$isRequestFetchAtom);
    _$isRequestFetchAtom.reportObserved();
    return super.isRequestFetch;
  }

  @override
  set isRequestFetch(bool value) {
    _$isRequestFetchAtom.context.conditionallyRunInAction(() {
      super.isRequestFetch = value;
      _$isRequestFetchAtom.reportChanged();
    }, _$isRequestFetchAtom, name: '${_$isRequestFetchAtom.name}_set');
  }

  final _$_LessonsStoreActionController =
      ActionController(name: '_LessonsStore');

  @override
  dynamic fetch() {
    final _$actionInfo = _$_LessonsStoreActionController.startAction();
    try {
      return super.fetch();
    } finally {
      _$_LessonsStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic setFetchFuture(Future<List<LessonItemView>> future) {
    final _$actionInfo = _$_LessonsStoreActionController.startAction();
    try {
      return super.setFetchFuture(future);
    } finally {
      _$_LessonsStoreActionController.endAction(_$actionInfo);
    }
  }
}
