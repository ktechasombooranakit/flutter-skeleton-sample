import 'package:flutter/material.dart';
import 'package:mobile/constants/style.dart';
import 'package:mobile/views/atoms/tex.dart';

class ContentCheatsheetListItem extends StatelessWidget {
  final int number;
  final String title;
  final List<String> texs;

  ContentCheatsheetListItem(
      {@required this.number, @required this.title, this.texs});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: defaultPadding),
        child: Padding(
          padding: const EdgeInsets.only(left: 10.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Container(
                    width: 30,
                    padding: const EdgeInsets.symmetric(vertical: 15.0),
                    child: Text(
                      "$number",
                      style: defaultTextStyle,
                    ),
                  ),
                  Expanded(
                    child: Text(
                      "$title",
                      overflow: TextOverflow.ellipsis,
                      style: defaultTextStyle,
                    ),
                  ),
                ],
              ),
              ...texs.map(
                (tex) => Padding(
                  padding: EdgeInsets.only(
                    left: 35.0,
                    bottom: defaultPadding,
                  ),
                  child: TexView(
                    tex: tex,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
