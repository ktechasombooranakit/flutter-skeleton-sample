abstract class Saga {
  const Saga();
  void start();
}

class MultiSaga extends Saga {
  final List<Saga> sagas;

  const MultiSaga({this.sagas});

  @override
  void start() {
    sagas.forEach((saga) => saga.start());
  }
}
