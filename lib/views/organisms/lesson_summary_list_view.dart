import 'package:flutter/material.dart';
import 'package:mobile/views/molecules/list_item/content_list_item.dart';

class LessonSummaryListView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: Connect to the store.
    return CustomScrollView(
      slivers: <Widget>[
        SliverList(
          delegate: SliverChildBuilderDelegate(
            (BuildContext context, int index) {
              if (index > 5) return null;
              return ContentListItem(
                title: "Test Title",
                number: index,
                duration: Duration(minutes: 12321312),
              );
            },
          ),
        ),
      ],
    );
  }
}
