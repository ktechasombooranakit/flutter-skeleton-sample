import 'package:flutter/material.dart';

class Button extends StatelessWidget {
  final Widget child;
  final Function onPress;

  const Button({this.child, this.onPress});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(onTap: onPress, child: child);
  }
}
