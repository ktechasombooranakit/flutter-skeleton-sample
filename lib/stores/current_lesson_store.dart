import 'package:mobile/stores/models/lesson_detail.dart';
import 'package:mobx/mobx.dart';
part 'current_lesson_store.g.dart';

class CurrentLessonStore = _CurrentLessonStore with _$CurrentLessonStore;

abstract class _CurrentLessonStore with Store {
  @observable
  String currentLessonId;

  @observable
  ObservableFuture<LessonDetail> lessonDetailFuture;

  @computed
  bool get hasCurrentLesson => currentLessonId == null;

  @computed
  bool get isLoaded =>
      lessonDetailFuture != null &&
      lessonDetailFuture.status == FutureStatus.fulfilled;

  @computed
  LessonDetail get lessonDetail {
    if (!isLoaded) {
      return null;
    }
    return lessonDetailFuture.value;
  }

  @computed
  Summary get summary => lessonDetail?.summary;

  @computed
  Content get content {
    return lessonDetail?.content;
  }

  @computed
  Cheatsheet get cheatsheet => lessonDetail?.cheatsheet;

  @computed
  String get title => lessonDetail?.title;

  @computed
  Duration get totalDuration => lessonDetail?.totalDuration;

  @action
  void setAndFetchCurrentLesson(String lessonId) {
    currentLessonId = lessonId;
  }

  @action
  void setCurrentLessonFuture(Future<LessonDetail> future) {
    lessonDetailFuture = ObservableFuture(future);
  }
}
