import 'package:flutter/material.dart';
import 'package:mobile/constants/style.dart';

class VideoViewLayout extends StatelessWidget {
  final Widget video;
  final Widget body;

  VideoViewLayout({this.video, this.body});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: foregroundColor,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Container(
            color: Colors.black,
            child: SafeArea(
              bottom: false,
              child: AspectRatio(
                aspectRatio: 16 / 9,
                child: video,
              ),
            ),
          ),
          Expanded(child: body)
        ],
      ),
    );
  }
}
