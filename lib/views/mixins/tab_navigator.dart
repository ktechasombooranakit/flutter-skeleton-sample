import 'package:flutter/material.dart';

typedef Widget NavigatorTabPageBuilder(
    BuildContext context, ScrollController scrollController);

class TabNavigatorView extends StatefulWidget {
  final TabNavigatorController controller;
  final NavigatorTabPageBuilder builder;

  TabNavigatorView({this.controller, @required this.builder});

  @override
  State<StatefulWidget> createState() {
    return _TabNavigatorViewState(builder: builder, controller: controller);
  }
}

class _TabNavigatorViewState extends TabPageState<TabNavigatorView> {
  ScrollController scrollController;
  NavigatorTabPageBuilder builder;

  _TabNavigatorViewState(
      {TabNavigatorController controller, @required this.builder})
      : this.scrollController = ScrollController(),
        super(controller);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      bottom: false,
      child: builder(context, scrollController),
    );
  }

  @override
  void tryTabPop() {
    scrollController.animateTo(0,
        duration: Duration(milliseconds: 250), curve: Curves.easeIn);
  }

  @override
  bool canTabPop() {
    var isReachedTop =
        scrollController.offset <= scrollController.position.minScrollExtent &&
            !scrollController.position.outOfRange;
    return !isReachedTop;
  }
}

abstract class TabPageState<T extends StatefulWidget> extends State<T>
    with _TabNavigatorState {
  TabPageState(TabNavigatorController tabNavigatorController) {
    setTabNavigatorController(tabNavigatorController);
  }
}

class TabNavigatorController {
  _TabNavigatorState _state;
  void pop() {
    _state?.tryTabPop();
  }

  bool get canPop => _state == null ? false : _state.canTabPop();
}

abstract class _TabNavigatorState {
  setTabNavigatorController(TabNavigatorController controller) {
    controller._state = this;
  }

  void tryTabPop();

  bool canTabPop();
}
