import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:mobile/views/mixins/tab_navigator.dart';

typedef Widget BottomTabbarPageBuilder(
    BuildContext context, TabNavigatorController controller, int index);

class BottomTabbarLayout extends StatefulWidget {
  final BottomTabbarPageBuilder builder;
  final List<BottomNavigationBarItem> tabItems;

  BottomTabbarLayout({this.tabItems, this.builder});

  @override
  _BottomTabbarLayoutState createState() =>
      _BottomTabbarLayoutState(tabItems: tabItems, builder: builder);
}

class _BottomTabbarLayoutState extends State<BottomTabbarLayout> {
  BottomTabbarPageBuilder builder;
  List<BottomNavigationBarItem> tabItems;
  List<TabNavigatorController> controllers;
  int currentTabIndex;

  _BottomTabbarLayoutState({this.tabItems, this.builder});

  @override
  void initState() {
    super.initState();
    currentTabIndex = 0;
    controllers = tabItems.map((item) => TabNavigatorController()).toList();
  }

  onTapped(int index) {
    if (currentTabIndex == index) {
      var controller = controllers[index];
      if (controller.canPop) {
        controllers[index].pop();
      }
    }
    setState(() {
      currentTabIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoTabScaffold(
      tabBar: CupertinoTabBar(
        onTap: onTapped,
        items: widget.tabItems,
      ),
      tabBuilder: (context, index) {
        return CupertinoTabView(
          builder: (context) =>
              widget.builder(context, controllers[index], index),
        );
      },
    );
  }
}
