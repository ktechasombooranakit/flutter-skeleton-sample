import 'package:flutter/material.dart';
import 'package:mobile/constants/style.dart';
import 'package:mobile/utils/extensions.dart';

class ContentListItem extends StatelessWidget {
  final int number;
  final String title;
  final Duration duration;
  final bool isDone;
  final bool isActive;

  ContentListItem({
    @required this.number,
    @required this.title,
    this.isDone,
    this.duration,
    this.isActive,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: defaultPadding),
        child: Padding(
          padding: const EdgeInsets.only(left: 10.0),
          child: Row(
            // crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Container(
                width: 30,
                padding: const EdgeInsets.symmetric(vertical: 15.0),
                child: Text(
                  "$number",
                  style: defaultTextStyle,
                ),
              ),
              Container(
                padding: EdgeInsets.only(right: 5),
                child: Icon(
                  Icons.check_circle,
                  color: Colors.green,
                  size: 20,
                ),
              ),
              Expanded(
                child: Text(
                  "$title",
                  overflow: TextOverflow.ellipsis,
                  style: defaultTextStyle,
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 8),
                child: Text(
                  "${DurationExtensions.toStringFormat(duration, "mm.ss")} mins",
                  textAlign: TextAlign.end,
                  style: defaultDescriptionTextStyle,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
