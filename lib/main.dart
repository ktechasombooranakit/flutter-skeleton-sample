import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mobile/sagas/main_saga.dart';
import 'package:mobile/sagas/saga.dart';
import 'package:mobile/stores/app_store.dart';
import 'package:mobile/stores/current_lesson_store.dart';
import 'package:mobile/stores/lessons_store.dart';
import 'package:mobile/views/main.dart';
import 'package:mobx/mobx.dart';
import 'package:provider/provider.dart';

var lessonsStore = LessonsStore();
var currentLessonStore = CurrentLessonStore();
var appStore = AppStore();
Saga mainSaga;

void main() {
  startSagas();
  startApp();
}

void startSagas() {
  // TODO : We should go with dependency injection when we more store.
  mainSaga = MainSaga(
    lessonsStore: lessonsStore,
    appStore: appStore,
    currentLessonStore: currentLessonStore,
  )..start();
}

void startApp() {
  runApp(
    buildProvider(
      child: MainApp(),
    ),
  );
  appStore.startup();
}

Widget buildProvider({Widget child}) {
  return MultiProvider(
    providers: [
      createStoreProvider(lessonsStore),
      createStoreProvider(currentLessonStore),
      createStoreProvider(appStore),
    ],
    child: child,
  );
}

Provider<T> createStoreProvider<T extends Store>(T value) {
  return Provider<T>(
    builder: (_) => value,
    dispose: (c, store) => store.dispose(),
  );
}
