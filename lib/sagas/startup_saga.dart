import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mobile/sagas/saga.dart';
import 'package:mobile/stores/app_store.dart';
import 'package:mobile/stores/lessons_store.dart';
import 'package:mobx/mobx.dart';

class StartupSaga extends Saga {
  final LessonsStore lessonsStore;
  final AppStore appStore;

  StartupSaga({@required this.lessonsStore, @required this.appStore});

  @override
  void start() {
    when((_) => appStore.hasStartup, () {
      setupStatusBarTheme();
      fetchAppRequiredData();
    });
  }

  void setupStatusBarTheme() {
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle.dark.copyWith(
          statusBarColor: Colors.white, // Color for Android
          statusBarBrightness:
              Brightness.dark // Dark == white status bar -- for IOS.
          ),
    );
  }

  void fetchAppRequiredData() {
    lessonsStore.fetch();
  }
}
